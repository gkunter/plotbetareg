# Copyright (c) 2015 Gero Kunter
# See https://bitbucket.org/gkunter/plotbetareg/src/master/LICENSE for
# conditions.

my.dbeta <- function (x, mu, phi) {
    if (phi > 171) {
        warning("phi too large for gamma function")
        NULL
    } else 
    # calculate the beta density using the parameterization in 
    # Cribari-Neto & Zeileis (2010:3):
    (gamma (phi) / (gamma (mu * phi) * gamma ((1 - mu) * phi))) * x ^ (mu * phi - 1) * (1 - x) ^ ((1 - mu) * phi - 1)
}

plotBetareg <- function (model, predictor, intr=NA, bins=NULL, parameters=T, 
xlab=NA, ylab=NA, srt=NA, mex=1, xlim=NULL, ylim=c(0, 1), pch=19, alpha=0.5, fill=NULL, lty=NULL, x_adj=0, x_pad=1, x_scale=1, legend.bg="white", legend="topleft", ... ) {
    PrepareMatrix <- function (model, component) {
        if (component == "mean")
            coef <- model$coefficients$mean
        else
            coef <- model$coefficients$precision
        data <- model$model
        matr <- diag (length (coef))
        rownames (matr) <- names (coef)
        matr [, 1] <- 1
        # adjust numeric predictors to their mean:
        numericPredictors <- which (!is.na (pmatch (names (coef), names (data))))
        for (i in numericPredictors)
            matr [, i] <- mean (data [, names (coef) [i]])
        invisible(matr)
    }
    
    GetAdjustedCoefficients <- function (model, predictor, component) {
        if (component == "mean")
            coef <- model$coefficients$mean
        else
            coef <- model$coefficients$precision
        matr <- PrepareMatrix (model, component)
        estimates <- as.vector (matr %*% coef)
        names (estimates) <- names (coef)
        estimates
    }

    GetAdjustedIntercept <- function (model, predictor, component) {
        if (component == "mean")
            coef <- model$coefficients$mean
        else
            coef <- model$coefficients$precision
        matr <- PrepareMatrix (model, component)
        if (predictor %in% names (coef))
            matr [, which (names (coef) == predictor)] <- 0
        as.vector (matr %*% coef) [1]
    }
    
    get_label <- function(s1, s2) {
        ifelse(s2 == "(Intercept)", 
            s1,
            ifelse(s1 == "(Intercept)", 
                s2,
                sprintf("%s:%s", s1, s2)))
    }
    
    data <- model$model
    pred <- data [, predictor]

    if (abs(x_adj) > 1) {
        warning("Illegal parameter: x_adj has to be between 0 and 1")
        x_adj <- sign(x_adj)
    }
    x_adj <- x_adj * 0.5
    
    if (is.na (xlab))
        xlab <- predictor
    if (is.na (ylab))
        ylab <- all.vars (model$formula [[2]])
    
    if (is.numeric(pred))
        if (is.null(bins)) 
           bins <- mean (pred) + sd (pred) * c(-2, -1, 0, 1, 2)
    
    if (!is.na(intr)) {
        if (is.numeric(pred))
            tab <- expand.grid(factor(bins), levels(data[, intr]))
        else
            tab <- expand.grid(levels(pred), levels(data[, intr]))
        colnames(tab)[2] <- intr
    } else {
        if (is.numeric(pred))
            tab <- expand.grid(factor(bins))
        else
            tab <- expand.grid(levels(pred))
    }
    colnames(tab)[1] <- predictor
    tab$mu <- rep(0, nrow(tab))
    tab$phi <- rep(0, nrow(tab))
    
    # plot for factors:
    if (is.factor (pred) | is.character (pred)) {
        predLength <- nlevels(pred)
        mean.coefficients <- GetAdjustedCoefficients (model, predictor, "mean")
        precision.coefficients <- GetAdjustedCoefficients (model, predictor, "precision")
        if (!is.na(intr))
            lev <- c("(Intercept)", levels(data[, intr])[2:nlevels(data[, intr])])
        else 
            lev <- c("(Intercept)")
        factor_levels <- rep(NA, predLength)
        factor_levels[1] <- "(Intercept)"
        axis_labels <- levels(pred)
        for (i in 2:predLength) {
            factor_levels[i] <- paste (predictor, levels (pred) [i], sep="")
        }
        
        count_level <- 1
        for (current_level in lev) {
            # take care of two-way interactions
            if (!is.na(intr) & current_level != "(Intercept)") {
                this_intr_level <- sprintf("%s%s", intr, current_level)
                intr_fact_lev <- paste(factor_levels, ":", this_intr_level, sep="")
                intr_fact_lev[1] <- this_intr_level
                intr_mu <- rowSums(
                    cbind (mean.coefficients[factor_levels],
                        model$coefficients$mean[intr_fact_lev]),
                    na.rm=T)
                intr_mu <- c(intr_mu[1], intr_mu[2:length(intr_mu)] + model$coefficients$mean[this_intr_level])
                if (length(model$coefficients$precision) == 1)
                    intr_phi <- rep(model$coefficients$precision, length(factor_levels))
                else {
                    intr_phi <- rowSums(
                        cbind (precision.coefficients[factor_levels],
                            model$coefficients$precision[intr_fact_lev]),
                        na.rm=T)
                    intr_phi <- c(intr_phi[1], intr_phi[2:length(intr_phi)] + model$coefficients$precision[this_intr_level])
                }
            } else {
            # estimate main effects:
                if (length(intersect(factor_levels, names(mean.coefficients))) == length(factor_levels))
                    intr_mu <- mean.coefficients[factor_levels]
                else
                    intr_mu <- rep(mean.coefficients["(Intercept)"], length(factor_levels))
                if (length(intersect(factor_levels, names(precision.coefficients))) == length(factor_levels))
                    intr_phi <- precision.coefficients[factor_levels]
                else {
                    if (length(model$coefficients$precision) == 1)
                        intr_phi <- rep(model$coefficients$precision, length(factor_levels))
                    else
                        intr_phi <- rep(precision.coefficients["(Intercept)"], length(factor_levels))
                }
            }
            if (is.na(intr)) {
                tab$mu <- plogis(intr_mu)
                tab$phi <- exp(intr_phi)
            } else {
                if (current_level == "(Intercept)")
                    tmp_label <- levels(data[, intr])[1]
                else
                    tmp_label <- current_level
                tab$mu [tab[, 2] == tmp_label] <- plogis(intr_mu)
                tab$phi [tab[, 2] == tmp_label] <- exp(intr_phi)
            }
        }
    } else {
    # plot for numeric variables
        mean.intercept  <- GetAdjustedIntercept (model, predictor, "mean")
        precision.intercept <- GetAdjustedIntercept (model, predictor, "precision")
        predLength <- length (bins)
        axis_labels <- sprintf("%s", round(bins, digits=as.numeric(options("digits"))))
        if (!is.na(intr)) {
            warning("Plotting interactions involving numeric predictors not supported.")
        } else {
            if (predictor %in% names(model$coefficients$mean))
                tab$mu <- mean.intercept + model$coefficients$mean[predictor] * bins
            else
                tab$mu <- rep(mean.intercept, length(bins))
            if (predictor %in% names(model$coefficients$precision))
                tab$phi <- precision.intercept + model$coefficients$precision[predictor] * bins
            else
                tab$phi <- rep(precision.intercept, length(bins))

        }
    }
    
    scaleDens <- x_scale * 0.95 / (2 * max(unlist(
        apply (tab[, c("mu", "phi")], 
            MARGIN=1, 
            FUN=function(x) my.dbeta(x[1], x[1], x[2])))))
    
    if (!is.na(intr))
        if (is.null(fill))
            fill <- rev(gray(0:nlevels(data[, intr])/nlevels(data[, intr]), alpha=alpha))
        
    if (is.null(lty))
        lty <- "solid"
    lty <- rep_len(lty, length(axis_labels))
    pch <- rep_len(pch, length(axis_labels))
    len <- length(axis_labels) - 1
    rel_x <- seq(len * -0.5, len * 0.5, 1) / len

    if (is.null(xlim))
        xlim <- c(1 - max(rel_x), predLength + max(rel_x)) * x_pad
    plot (1:predLength * x_pad, xaxt="n", type="n", xlim=xlim, ylim=ylim, ylab=ylab, xlab=xlab, ...)
    # try to place the labels nicely if srt != NA, i.e. with rotated text:
    if (!is.na(srt)) {
        x <- 1:predLength * x_pad
        y_pos <- 0.01 * (par("usr")[4] - par("usr")[3]) / (par("plt")[4] - par("plt")[3])
        y <- rep(par("usr")[3] - y_pos, length(axis_labels))
        text(x=x, y=y, axis_labels, srt=srt, xpd=T, adj=1, cex=mex)
    } else
        axis (1, at=(1:predLength) * x_pad, labels=axis_labels, ...)
    
    max_pred <- length(unique(tab[, 1]))
    if (!is.na(intr)) 
        max_intr <- length(unique(tab[, 2]))
    else
        max_intr <- 1
        
    for (i in 1:max_pred) {
        for (j in rev(1:max_intr)) {
            mu <- tab$mu[i + (j - 1) * max_pred]
            phi <- tab$phi[i + (j - 1) * max_pred]
            dens <- scaleDens * my.dbeta(seq(0, 1, 0.01), mu, phi)
            x_i <- (i + rel_x[j] * x_adj) * x_pad
            if (!is.null(dens)) {
                non_zero <- sapply (dens, function(x) !isTRUE(all.equal(0, x)))
                if (length(non_zero) > 0) {
                    y_dat <- seq(0, 1, 0.01)[non_zero]
                    polygon (x_i + dens[non_zero], y_dat, col=fill[j], border=NA)
                    polygon (x_i - dens[non_zero], y_dat, col=fill[j], border=NA)
                    lines (x_i + dens[non_zero], y_dat, lty=lty[j], ...)
                    lines (x_i - dens[non_zero], y_dat, lty=lty[j], ...)
                }
            }
            points (x_i, mu, pch=pch[j])
        }
    }

    if (!is.na(legend))
        if (!is.na(intr))
            if (!is.na(fill))
                legend (legend, fill=fill, lty=lty, legend=levels(data[, intr]), bg=legend.bg, pch=pch)
            else
                legend (legend, lty=lty, legend=levels(data[, intr]), bg=legend.bg, pch=pch)

    invisible(tab)
}
