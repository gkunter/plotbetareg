# README #

This is a plotting function for beta regression models that were fitted using the R package [betareg](http://cran.r-project.org/web/packages/betareg/index.html).

### Usage ###
```
#!r
plotBetareg (model, predictor, intr=NA, bins=NULL, parameters=T, 
    xlab=NA, ylab=NA, srt=NA, mex=1, xlim=NULL, ylim=c(0, 1), pch=19, 
    alpha=0.5, fill=NULL, lty=NULL, x_adj=0, x_pad=1, x_scale=1, 
    legend.bg="white", mex=1, legend="topleft", ... )
```

### Arguments ###
* **model** A 'betareg' model object
* **predictor** A character string which specifies the name of the predictor to be plotted.
* **intr** A character string which specifies the name of an interaction term. Leave empty if no interaction is requested.
* **bins** A list of values that are used as bins for a numeric predictors (selected automatically if not specified).
* **parameters** A logical value. If TRUE (default), estimated parameters will be printed.
* **xlab**, **ylab** A character string that specifies the title for the horizontal and vertical axis, respectively.
* **srt** A numeric variable that specifies the rotation of the horizontal labels in degrees (default: no rotation).
* **mex** A numeric variable that specifies the character expansion factor for the horizontal labels (default: 1.0, i.e. no expansion).
* **xlim** A numeric value that specifies the horizontal plotting range (automatically selected if not specified).
* **ylim** A numeric value that specifies the vertical plotting range, usually between 0.0 and 1.0 (which is also the default).
* **pch** An integer value or a single character that is used as the plotting character (default: 19). See 'points()' for details.
* **alpha** A numeric value between 0.0 and 1.0 that determines the degree of transparency of the filling color (default: 0.5).
* **fill** The color that is used to fill the violin plots (default: select a different shade of gray for each tick on the horizontal axis).
* **lty** The line type of the border (see 'par()' for details).
* **x_adj** A numeric value that specifies the horizontal distance adjustment, relative to the following parameter **x_pad**. Can be used to fine-tune interaction plots.
* **x_pad** A numeric value that specifies the distance of ticks on the horizontal axis.
* **x_scale** A numeric value that adjusts the horizontal scaling of the violins (default is 1.0).
* **legend.bg** The color that is used as the background for the legend box.
* **legend** A character string that specifies the location of the legend box (see 'legend()' for details, or NA if no legend should be printed (default: "topleft").
* **...** Further graphical parameters (see 'par()') that are passed to the plotting functions.

### Value ###
A table containing the estimated beta parameters mu and phi for the plotted predictor variables.

### Details ###
This function plots violin plots based on the beta distribution for the mu and phi values as estimated by the beta regression model. If the predictor is a factor, the beta parameters mu and phi are calculated for each factor level. If the selected predictor is a numerical variable, the beta parameters mu and phi are calculated for values of X set to mean(X)-2*sd(X), mean(X)-sd(X), mean(X), mean(X)+sd(X), mean(X)+2*sd(X). A two-way interaction can be plotted by using the 'intr' argument.

The other predictors in the model are adjusted to their mean if they are numerical, or to the reference level if they are factors.

### Limitations ###

* The function can plot numerical predictors as well as factors, but interactions are limited interactions with a categorical predictor.
* All involved variables have to be contained in the data frame that is used to fit the beta regression model. On-the-fly calculations are not possible.
* This function was only tested for a small number of beta regression models. If you experience any unexpected results, feel free to open an Issue using the issue tracker on this site.

### Maintainer ###

* Gero Kunter
